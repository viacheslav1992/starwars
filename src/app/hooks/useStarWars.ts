import { useState, useEffect, useCallback } from 'react';
import { AxiosError } from 'axios'

import { starWarsService, StarWarsData } from '../services';

const useStarWars = () => {
  const [data, setData] = useState<StarWarsData>({} as StarWarsData);
  const [loading, setLoading] = useState(true);
  const [error, setError] = useState<AxiosError['message'] | null>(null);

  const fetchData = useCallback(async (url?: string) => {
    setLoading(true);
    try {
      let response = await starWarsService.characters({ url });
      setData(response as unknown as StarWarsData);
    } catch (error) {
      setError((error as AxiosError).message);
    } finally {
      setLoading(false);
    }
  }, []);

  useEffect(() => {
    fetchData();
  }, [fetchData]);

  const fetchMore = async (url: string) => {
    fetchData(url);
  };

  const clearErrors = () => {
    setError(null);
  };

  return { data, loading, error, clearErrors, fetchMore };
};

export default useStarWars;