'use client'

export default function Error({
  reset,
}: {
  error: Error
  reset: () => void
}) {

  return (
    <div className="p-[30px] text-center">
      <h2 className="text-xl font-extrabold">Something went wrong!</h2>
      <br />
      <br />
      <button className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded" onClick={reset}> Try again </button>
    </div>
  )
}