import Link from 'next/link';
import { notFound } from 'next/navigation';

import { starWarsService, StarWarsData, StarWarsCharacterType, StarWarsStarShipType } from '../services';
import { convertStringToDateFormat, fetchData } from '../utils';
import FaceIcon from 'public/assets/svg/face.svg';

type PageProps = {
  params: {
    slug: string
  }
}

type ExtendedStarWarsCharacterType = StarWarsCharacterType & {
  starShipsInfo: (StarWarsCharacterType & StarWarsStarShipType)[]
}

export default async function Page({ params }: PageProps) {
  const slugParam = params.slug.replaceAll("_", ' ');
  const character = await getData(slugParam) as ExtendedStarWarsCharacterType;

  if (Object.keys(character).length === 0) {
    notFound()
  }

  return <div className="flex flex-row flex-wrap items-start">
    <Link href="/" className='p-5'>
      &#8592; Back to Home page
    </Link>
    <div className='w-[100%] flex pt-6 pb-6 items-start justify-center flex-col lg:flex-row'>
      <div className="lg:w-1/3 mt-[1%] mb-[1%] pl-10 pr-10 flex items-start justify-center ">
        <span className='w-14 h-14 rounded-lg flex items-center justify-center bg-white shadow-lgs w-[200px] h-[200px]'>
          <FaceIcon />
        </span>
      </div>
      <div className="flex flex-col flex-nowrap items-start justify-center sm:justify-between pl-10 pr-10 lg:w-2/3">
        <h2 className='mb-2 text-2xl font-bold tracking-tight text-gray-900 dark:text-white'>Character: {character.name}</h2>
        <p>Birth year - {character.birth_year}</p>
        <p>Height - {character.height}</p>
        <p>Created - {convertStringToDateFormat(character.created)}</p>
        <p>Gender - {character.gender}</p>
        {character.starShipsInfo.length > 0 &&
          <ul className='mt-4'>
            <li><h4>Star Ships Info</h4></li>
            {character.starShipsInfo.map(starShip =>
              <li key={starShip.name}>
                <p>Name: {starShip.name}</p>
                <p>Model: {starShip.model}</p>
              </li>
            )}
          </ul>
        }
      </div>
    </div></div>
}

async function getData(slug: string) {
  let characterData = await starWarsService.searchByParam({ url: slug }) as StarWarsData;

  if (characterData.results.length < 1) {
    return []
  }
  const firstCharacter = characterData.results[0];

  let extendedCharacterData: ExtendedStarWarsCharacterType = {
    ...firstCharacter,
    starShipsInfo: []
  }

  if (firstCharacter.starships.length > 0) {
    const starShipsData = await fetchData(firstCharacter.starships);
    extendedCharacterData = {
      ...extendedCharacterData,
      starShipsInfo: starShipsData
    }
  }

  return extendedCharacterData;
}