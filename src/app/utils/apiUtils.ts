export const fetchData = async (urls: string[]) => {
  try {
    const responses = await Promise.all(urls.map(url => fetch(url)));
    const data = await Promise.all(responses.map(response => response.json()));
    return data;
  } catch (error) {
    throw new Error(`${error}`)
  }
};