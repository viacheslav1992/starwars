import starWarsService from './starWars';

import {
  StarWarsCharactersPropsType,
  StarWarsData,
  StarWarsCharacterType,
  StarWarsStarShipType
} from './starWars/types';

export {
  starWarsService,
};

export type {
  StarWarsCharactersPropsType,
  StarWarsData,
  StarWarsCharacterType,
  StarWarsStarShipType
};
