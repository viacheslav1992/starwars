export type StarWarsCharactersPropsType = {
  url?: string;
}

export type StarWarsServiceType = {
  characters: (props: StarWarsCharactersPropsType) => Promise<unknown>;
  searchByParam: (props: StarWarsCharactersPropsType) => Promise<unknown>;
}

export type StarWarsData = {
  count: number,
  next: null | string,
  previous: null | string,
  results: StarWarsCharacterType[]
}

export type StarWarsCharacterType = {
  name: string;
  birth_year: string;
  height: string;
  created: string;
  gender: string;
  starships: string[];
}

export type StarWarsStarShipType = {
  name: string;
  model: string;
}