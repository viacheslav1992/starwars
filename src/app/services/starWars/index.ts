import axios, { AxiosError, AxiosInstance } from 'axios';

import {
  StarWarsCharactersPropsType,
  StarWarsServiceType
} from './types';

const api: AxiosInstance = axios.create();
api.defaults.baseURL = process.env.NEXT_PUBLIC_STARWARS_API_URL;
api.interceptors.response.use(
  response => {
    return response;
  },
  error => {
    if (error instanceof AxiosError && error.response) {
      throw new Error(error.response.data.message);
    } else {
      throw new Error(error.message);
    }
  }
);

const starWarsService: StarWarsServiceType = {
  characters: async ({ url }: StarWarsCharactersPropsType = {}) => {
    try {
      const response = await api.get(url ? url : '/people/');
      return response.data;
    } catch (error: unknown) {
      throw new Error('Failed to get characters');
    }
  },
  searchByParam: async ({ url }: StarWarsCharactersPropsType = {}) => {
    try {
      const response = await api.get('/people/?search=' + url);
      return response.data;
    } catch (error: unknown) {
      throw new Error('Failed to get specific character');
    }
  },
};

export default starWarsService;
