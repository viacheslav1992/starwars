import './globals.css'
import type { Metadata } from 'next'
import { Inter } from 'next/font/google';
import { Toaster } from 'react-hot-toast';

import { Header, Footer } from './components';

const inter = Inter({ subsets: ['latin'] })

export const metadata: Metadata = {
  title: 'Star Wars App',
  description: 'Created by Viacheslav Boiarchuk',
}

export default function RootLayout({
  children,
}: {
  children: React.ReactNode
}) {
  return (
    <html lang="en">
      <body className={inter.className}>
        <Header />
        <main className={`flex justify-center items-start mb-auto`}>
          {children}
        </main>
        <Toaster />
        <Footer />
      </body>
    </html>
  )
}
