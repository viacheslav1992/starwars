'use client';

import { useEffect } from 'react';
import toast from 'react-hot-toast'

import useStarWars from "./hooks/useStarWars";
import { Loader, Characters, Sidebar, Pagination } from './components';

const StarWarsURL = process.env.NEXT_PUBLIC_STARWARS_API_URL;

export default function Home() {
  const { data: { count = 0, results = [] }, loading, error, clearErrors, fetchMore } = useStarWars();

  const handleChange = (url: number) => fetchMore(`${StarWarsURL}people/?page=${url + 1}`)

  const handleSearchChange = (searchParam?: string) => fetchMore(`${StarWarsURL}people/?search=${searchParam}`)

  useEffect(() => {
    if (error) {
      toast.error(`${error}`, {
        position: 'bottom-center',
      });
      clearErrors();
    }
  }, [error, clearErrors]);

  return (
    <>
      {loading && <Loader />}
      <div className="w-[100%] flex pt-6 pb-6 items-flex-start justify-center flex-col lg:flex-row">
        <Sidebar onChange={(searchParam) => handleSearchChange(searchParam)} />
        <div className="flex flex-row flex-wrap items-center justify-center sm:justify-between pl-10 pr-10 lg:w-2/3">
          <Characters collection={results} />
          {results.length > 0 &&
            <Pagination total={count} itemsPerPage={10} onChange={handleChange} />
          }
        </div>
      </div>
    </>
  )
}