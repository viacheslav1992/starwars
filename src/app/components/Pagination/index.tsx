"use client";

import React from 'react';
import ReactPaginate from 'react-paginate';

type PaginationProps = {
  onChange: (url: number) => void;
  total: number;
  itemsPerPage: number;
}

const Pagination = ({ onChange, total, itemsPerPage }: PaginationProps) => {
  const pageCount = Math.ceil(total / itemsPerPage);

  return (
    <div className="pt-5 pb-5 text-center w-[100%]">
      <ReactPaginate
        breakLabel="..."
        nextLabel="next >"
        pageRangeDisplayed={2}
        onPageChange={(e) => onChange(e.selected)}
        pageCount={pageCount}
        previousLabel="< previous"
        className="flex flex-row items-center justify-center gap-4"
      />
    </div>
  )
}

export default Pagination;