import Link from 'next/link';

import styles from './styles.module.css'

const Header = () => (
  <header className={`${styles.header} w-full p-9 bg-gray-50`}>
    <Link href="/" >
      <h1 className={`${styles.h1} text-center text-2xl font-extrabold`}>Star Wars Application</h1>
    </Link>
  </header>
)

export default Header;