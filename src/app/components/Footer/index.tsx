const Footer = () => (
  <footer className="z-50 flex justify-center w-full px-4 py-3 border border-b border-gray-200 bg-gray-50 dark:border-gray-600 lg:py-4 dark:bg-gray-700">
    <p className="text-sm font-medium text-gray-900 md:my-0 dark:text-white">
      Created by Viacheslav Boiarchuk. &copy;2023
    </p>
  </footer>
)

export default Footer;