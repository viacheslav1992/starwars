"use client";

import React from 'react'
import { useFormik } from 'formik';

import CloseIcon from 'public/assets/svg/close.svg';
import DandruffIcon from 'public/assets/svg/dandruff.svg';

type SidebarProps = {
  onChange: (param: string) => void;
}

const Sidebar = ({ onChange }: SidebarProps) => {

  const formik = useFormik({
    initialValues: {
      search: '',
    },
    onSubmit: (values) => {
      onChange(values.search);
    },
  });

  const handleReset = () => {
    formik.resetForm()
    onChange('');
  }

  return (
    <div className="lg:w-1/3 mt-[1%] mb-[1%] pl-10 pr-10">
      <form onSubmit={formik.handleSubmit}>
        <label htmlFor="search" className="mb-2 text-sm font-medium text-gray-900 sr-only dark:text-white">Search</label>
        <div className="relative">
          <div className="absolute inset-y-0 left-0 flex items-center pl-3 pointer-events-none">
            <DandruffIcon />
          </div>
          <input
            id="search"
            name="search"
            type="text"
            onChange={formik.handleChange}
            value={formik.values.search}
            className="block w-full p-4 pl-10 pr-[150px] text-sm text-gray-900 border border-gray-300 rounded-lg bg-gray-50 focus:ring-blue-500 focus:border-blue-500 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500" placeholder="Search Mockups, Logos..." required />
          {formik.values.search &&
            <span role="presentation"
              className="absolute inset-y-0 flex cursor-pointer items-center right-[100px]"
              onClick={handleReset}>
              <CloseIcon />
            </span>}
          <button type="submit" className="text-white absolute right-2.5 bottom-2.5 bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm px-4 py-2 dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800">Search</button>
        </div>
      </form>
    </div>
  )
}

export default Sidebar;