import Link from 'next/link';

import { StarWarsCharacterType } from '../../../../services';
import { convertStringToDateFormat } from '../../../../utils';
import styles from './styles.module.css';

type CharactersProps = {
  collection: StarWarsCharacterType[];
}

const Characters = ({ collection }: CharactersProps) => {
  return collection.map(({ name, birth_year, height, created }) => (
    <div key={`${name}`}
      className={`${styles.item} w-[100%] sm:w-[48%] min-h-300 text-center max-w-sm bg-white border border-gray-200 rounded-lg shadow dark:bg-gray-800 dark:border-gray-700 m-[1%]`}>
      <Link href={`/${name.replaceAll(" ", "_")}`} className="block h-[100%]">
        <div className="p-5">
          <h3 className="mb-2 text-2xl font-bold tracking-tight text-gray-900 dark:text-white">{name}</h3>
          <p>Birth year - {birth_year}</p>
          <p>Height - {height}</p>
          <p>Created - {convertStringToDateFormat(created)}</p>
        </div>
      </Link>
    </div>
  ))
}
export default Characters;