import Loader from "./Loader";
import Characters from "./pages/home/Character";
import Sidebar from "./Sidebar";
import Header from "./Header";
import Footer from "./Footer";
import Pagination from "./Pagination";

export { Loader, Characters, Sidebar, Header, Footer, Pagination };