import LoaderIcon from 'public/assets/svg/loader.svg';
import styles from './styles.module.css';

const Loader = () => (
  <div role="status" className={styles.loader}>
    <span className={`${styles.icon} w-8 h-8 mr-2 text-gray-200 dark:text-gray-600`}>
      <LoaderIcon />
    </span>
    <span className="sr-only">Loading...</span>
  </div>
)

export default Loader;