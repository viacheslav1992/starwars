## Before starting

To show different approaches, first page was created using client-side fetching, second page - using server-side fetching. 

Because NextJs from the box supports TailwindCss, I decided to use it for styling.

## Getting Started

First, install npm packages:

```bash
npm i
```

Second, run the development server:

```bash
npm run dev
```

Open [http://localhost:3000](http://localhost:3000) with your browser to see the result.